import * as React from "react";
import { Album } from "src/model/album.interface";

interface AlbumsGridProps {
  albums: Album[];
}

export const AlbumsGrid: React.SFC<AlbumsGridProps> = props => {
  return (
    <div className="card-group">
      {props.albums.map(album => (
        <div
          className="card"
          key={album.id}
          style={{
            flex: "0 0 25%"
          }}
        >
          <img className="card-img-top" src={album.images[0].url} />
          <div className="card-body">
            <h5 className="card-title">{album.name}</h5>
          </div>
        </div>
      ))}
    </div>
  );
};
