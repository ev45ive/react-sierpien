import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import MusicSearchView, { DispatchProps } from "../MusicSearchView";
import { fetchAlbums } from "../../reducers/albums-search.reducer";
import { MusicSearchViewProps } from "../MusicSearchView";
import { selectAlbumsList } from "../../reducers/albums-search.reducer";
import { AppState } from "../../store";
import { RouteComponentProps } from "react-router";

const mapStateToProps: MapStateToProps<
  MusicSearchViewProps,
  {
    token: string;
  } & RouteComponentProps<{}>,
  AppState
> = (state: AppState, ownProps) => {
  return {
    albums: selectAlbumsList(state.albums),
    query: state.albums.query,
    token: ownProps.token,
    ...ownProps
  };
};

const mapDispatchToProps: MapDispatchToProps<
  DispatchProps,
  {
    token: string;
  }
> = (dispatch, ownProps) => ({
  fetchAlbums: fetchAlbums(dispatch)
});

export const MusicSearch = connect(
  mapStateToProps,
  mapDispatchToProps
)(MusicSearchView);
