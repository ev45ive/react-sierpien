import * as React from "react";
import { ListFilter } from "../components/ListFilter";
import { Album } from "../model/album.interface";
import { AlbumsGrid } from "./AlbumsGrid";
import { RouteComponentProps } from "react-router";

export interface MusicSearchViewProps extends RouteComponentProps<{}> {
  token: string;
  query: string;
  albums: Album[];
}

export interface DispatchProps {
  fetchAlbums(query: string, token: string): void;
}

export default class MusicSearchView extends React.Component<
  MusicSearchViewProps & DispatchProps
> {
  searchRef = React.createRef<HTMLInputElement>();

  search = (query: string) => {

    this.props.history.replace({
      search: "q=" + query
    });
    this.fetchAlbums(query);
  };

  componentDidMount() {
    this.fetchAlbums("batman");
  }

  fetchAlbums(query: string) {
    this.props.fetchAlbums(query, this.props.token);
  }

  public render() {
    return (
      <>
        <div className="row">
          <div className="col">
            <h3>Music Search</h3>
            <ListFilter
              filter={this.props.query}
              onChange={this.search}
              inputRef={this.searchRef}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <AlbumsGrid albums={this.props.albums} />
          </div>
        </div>
      </>
    );
  }
}
