import { createStore, combineReducers } from "redux";
import { counter, increment, decrement } from "./reducers/counter.reducer";
import { albumsSearchState, albums } from "./reducers/albums-search.reducer";
import {
  playlists,
  playlistsState,
  actions
} from "./reducers/playlists.reducer";

export interface AppState {
  counter: number;
  playlists: playlistsState;
  albums: albumsSearchState;
}

const reducer = combineReducers<AppState>({
  playlists,
  counter,
  albums
});

export const store = createStore(
  reducer,
  window["__REDUX_DEVTOOLS_EXTENSION__"] &&
    window["__REDUX_DEVTOOLS_EXTENSION__"]()
);

console.log(store, increment, decrement, actions);

export const selectPlaylistsState = (state: AppState) => state.playlists;
