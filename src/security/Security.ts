/**
 *
 */
export class Security {
  authorize() {
    const url = "https://accounts.spotify.com/authorize";
    const client_id = "93c3ceb839b84bd2af1d85f572631e4b";
    const response_type = "token";
    const redirect_uri = "http://localhost:3000/";

    const redirect = `${url}?client_id=${client_id}&response_type=${response_type}&redirect_uri=${redirect_uri}`;

    localStorage.removeItem('token')

    window.location.replace(redirect);
  }

  extractToken() {
    const params = window.location.hash.substring(1);
    const match = params.match(/access_token=([^&]+)/);

    return (match && match[1]) || "";
  }

  token: string = "";

  getToken() {
    this.token = JSON.parse(localStorage.getItem("token") || "null");

    if (!this.token) {
      this.token = this.extractToken();
      localStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
