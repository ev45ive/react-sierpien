import * as React from "react";
import { Security } from "./Security";

export const security = new Security();

export const SecurityContext = React.createContext<Security>(security);

// export const Consumer = securityContext.Consumer;
// export const Provider = securityContext.Provider;

// export const { Provider, Consumer } = securityContext;
