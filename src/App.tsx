import * as React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Layout } from "./components/Layout";
import { Route, Switch, Redirect } from "react-router";
import { Playlists } from "./playlists/containers/Playlists";
import { SecurityContext } from "./security/SecurityProvider";
import { MusicSearch } from "./music/containers/MusicSearch";

interface AppState {}

class App extends React.Component<{}, AppState> {
  public render() {
    return (
      <Layout>
        <div>
          <Switch>
            <Redirect from="/" exact={true} to="playlists" />
            <Route path="/playlists" exact={true} component={Playlists} />
            <Route
              path="/music"
              render={routerProps => {
                return (
                  <SecurityContext.Consumer>
                    {security => (
                      <MusicSearch
                        {...routerProps}
                        token={security.getToken()}
                      />
                    )}
                  </SecurityContext.Consumer>
                );
              }}
            />
            <Route render={() => <p>Nothing here ...</p>} />
          </Switch>
        </div>
      </Layout>
    );
  }
}

export default App;
