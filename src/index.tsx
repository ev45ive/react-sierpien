import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./App";
import "./index.css";
import { SecurityContext, security } from "./security/SecurityProvider";
import { store } from "./store";
import { Provider } from "react-redux";
// import registerServiceWorker from './registerServiceWorker';
// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

const token = security.getToken();

if (token) {
  store.dispatch({
    type: "SET_TOKEN",
    payload: token
  });
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <SecurityContext.Provider value={security}>
        <App />
      </SecurityContext.Provider>
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
// registerServiceWorker();

window["React"] = React;
window["ReactDOM"] = ReactDOM;
