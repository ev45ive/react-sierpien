import * as React from "react";

interface NavItemProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  selected: boolean;
  // onClick(event: React.MouseEvent): void;
}

export const NavItem: React.SFC<NavItemProps> = props => {
  return (
    <li className="nav-item">
      <a
        {...props}
        className={`nav-link ${
          props.selected ? "active" : ""
        } ${props.className || ""}`}
        href="#"
      >
        {props.children}
      </a>
    </li>
  );
};

// tsrsfc
