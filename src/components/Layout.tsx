import * as React from "react";
import { StatelessComponent } from "react";
import { NavLink } from "react-router-dom";

export const Layout: StatelessComponent = props => {
  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="#/">
            Music App
          </a>
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/music">
                  Music
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">{props.children}</div>
        </div>
      </div>
    </>
  );
};
