import * as React from "react";

export interface NavTabsProps {}
export interface NavTabsState {
  selected: string;
}

export class NavTabs extends React.Component<NavTabsProps, NavTabsState> {
  state = {
    selected: ""
  };
  select = (selected: string) => {
    this.setState({ selected });
  };

  public render() {
    return (
      <div>
        <ul className="nav">
          {React.Children.map(
            this.props.children,
            (tab: React.ReactElement<NavTab>) => {
              return (
                <li
                  key={tab.props["title"]}
                  className="nav-item"
                  onClick={() => this.select(tab.props["title"])}
                >
                  <a className="nav-link" href="#">
                    {tab.props["title"]}
                  </a>
                </li>
              );
            }
          )}
        </ul>

        {React.Children.toArray(this.props.children)
          .filter(
            (tab: React.ReactElement<NavTab>) =>
              tab.props["title"] == this.state.selected
          )
          .map((tab: React.ReactElement<NavTab>) => {
            return <div key={1}>{tab.props["children"]}</div>;
          })}
      </div>
    );
  }
}

export interface NavTabProps {
  title: string;
}

export class NavTab extends React.Component<NavTabProps, any> {
  public render() {
    return <div />;
  }
}
