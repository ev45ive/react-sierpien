import * as React from "react";

export interface ListFilterProps {
  filter: string;
  inputRef: React.RefObject<HTMLInputElement>;
  onChange(query: string): void;
}

export const ListFilter = (props: ListFilterProps) => (
  <div className="mb-2">
    <input
      type="search"
      ref={props.inputRef}
      className="form-control"
      value={props.filter}
      onChange={e => props.onChange(e.target.value)}
    />
  </div>
);
