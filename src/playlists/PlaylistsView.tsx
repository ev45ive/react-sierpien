import * as React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Playlist } from "../model/playlist.interface";
import ItemsList from "./ItemsList";
import PlaylistDetails from "./PlaylistDetails";
import { ListFilter } from "../components/ListFilter";

export interface PlaylistsViewProps {
  selected?: Playlist;
  filter: string;
  playlists: Playlist[];
}
export interface DispatchProps {
  playlistSelected(playlistId: Playlist["id"]): void;
  playlistUpdated(playlist: Playlist): void;
  playlistFilter(query: string): void;
}

export class PlaylistsView extends React.Component<
  PlaylistsViewProps & DispatchProps
> {
  select = (selected: Playlist) => {
    this.props.playlistSelected(selected.id);
  };

  save = (playlist: Playlist) => {
    this.props.playlistUpdated(playlist);
  };

  componentDidMount() {
    this.focus();
  }

  focus() {
    const ref = this.filterInputRef;
    if (ref.current) {
      ref.current.focus();
    }
  }
  filterInputRef = React.createRef<HTMLInputElement>();

  filter = (query: string) => {
    this.props.playlistFilter(query);
  };

  public render() {
    const playlists = this.props.playlists;
    return (
      <div className="row">
        <div className="col">
          <ListFilter 
          filter={this.props.filter}
          onChange={this.filter} inputRef={this.filterInputRef} />
          <ItemsList
            ref={e => /* console.log */ e}
            onSelect={this.select}
            items={playlists}
            selected={this.props.selected}
          />
        </div>
        <div className="col">
          {this.props.selected ? (
            <PlaylistDetails
              onSave={this.save}
              playlist={this.props.selected}
            />
          ) : (
            <p>Please select playlist</p>
          )}
        </div>
      </div>
    );
  }
}
