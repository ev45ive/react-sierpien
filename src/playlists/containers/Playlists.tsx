import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { AppState } from "../../store";
import {
  actions,
  selectFilteredPlaylists
} from "../../reducers/playlists.reducer";
import {
  PlaylistsViewProps,
  PlaylistsView,
  DispatchProps
} from "../PlaylistsView";
import { selectSelectedPlaylist } from "src/reducers/playlists.reducer";
import { bindActionCreators } from "redux";

const mapStateToProps: MapStateToProps<
  // Props from state:
  PlaylistsViewProps,
  // Own Props (not from state):
  {},
  // Store State:
  AppState
> = state => ({
  // playlists: selectPlaylistsList(state.playlists),
  playlists: selectFilteredPlaylists(state.playlists),
  filter: state.playlists.filter,
  selected: selectSelectedPlaylist(state.playlists)
});

// const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = dispatch => ({
//   playlistSelected: id => dispatch(actions.playlistSelected(id))
// });

const mapDispatchToProps: MapDispatchToProps<DispatchProps, {}> = dispatch =>
  bindActionCreators(
    {
      playlistSelected: actions.playlistSelected,
      playlistUpdated: actions.playlistUpdated,
      playlistFilter: actions.playlistFilter
    },
    dispatch
  );

const enhancer = connect(
  mapStateToProps,
  mapDispatchToProps
);

export const Playlists = enhancer(PlaylistsView);
