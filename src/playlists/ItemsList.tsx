import * as React from "react";
import { Playlist } from "../model/playlist.interface";

export interface ItemsListProps {
  items: Playlist[];
  selected?: Playlist;
  onSelect: (selected: Playlist) => void;
}

export default class ItemsList extends React.PureComponent<ItemsListProps> {
  select(item: Playlist) {
    this.props.onSelect(item);
  }
  
/*   shouldComponentUpdate(
    nextProps: ItemsListProps
  ) {
    return (this.props.items != nextProps.items) || (this.props.selected != nextProps.selected);
  } */

  public render() {
    const { selected, items } = this.props;

    return (
      <div className="list-group">
        {items.map((item, index) => (
          <div
            className={`list-group-item ${
              item.id == (selected && selected.id) ? "active" : ""
            }`}
            key={item.id}
            /*  */
            onClick={() => this.select(item)}
          >
            {item.name}
          </div>
        ))}
      </div>
    );
  }
}
