import * as React from "react";
import { Playlist } from "../model/playlist.interface";

export interface PlaylistDetailsProps {
  playlist: Playlist;
  onSave: (playlist: Playlist) => void;
}

export interface PlaylistDetailsState {
  isEditing: boolean;
  playlist?: Playlist;
}

export default class PlaylistDetails extends React.PureComponent<
  PlaylistDetailsProps,
  PlaylistDetailsState
> {
  state: PlaylistDetailsState = {
    isEditing: false
  };
  edit = (event: React.MouseEvent) => {
    this.setState({
      isEditing: true
      // playlist: { ...this.props.playlist }
    });
  };

  static getDerivedStateFromProps(
    props: PlaylistDetailsProps,
    state: PlaylistDetailsState
  ) {
    // console.log("getDerivedStateFromProps");
    return {
      playlist: state.isEditing ? state.playlist : props.playlist
    };
  }

  show = () => {
    this.setState({
      isEditing: false
    });
  };

  save = () => {
    if (this.state.playlist) {
      this.props.onSave(this.state.playlist);
      this.show();
    }
  };

  fieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const elem = event.target;
    const value = elem.type == "checkbox" ? elem.checked : elem.value;
    const name = elem.name;

    // if(this.state.playlist){
    //   this.state.playlist[name] = value;
    //   this.setState({})
    // }

    this.setState(state => {
      return state.playlist
        ? {
            playlist: {
              ...state.playlist,
              [name]: value
            }
          }
        : {};
    });
  };

  componentDidMount() {
    // console.log("componentDidMount");
  }

  /*   shouldComponentUpdate(
    nextProps: PlaylistDetailsProps,
    nextState: PlaylistDetailsState
  ) {
    // console.log("shouldComponentUpdate");
    return (this.props.playlist != nextProps.playlist)
    || (this.state != nextState);
  } */
  // componentWillUpdate
  getSnapshotBeforeUpdate() {
    // console.log("getSnapshotBeforeUpdate");
    return { scrollPos: 0 };
  }

  componentDidUpdate(
    props: PlaylistDetailsProps,
    state: PlaylistDetailsState,
    snapshot: any
  ) {
    console.log("componentDidUpdate");
  }

  public render() {
    if (!this.props.playlist) {
      return <p>Please select playlist</p>;
    }

    return !this.state.isEditing ? (
      <div>
        <dl>
          <dt>Name:</dt>
          <dd>{this.props.playlist.name}</dd>
          <dt>Favourite:</dt>
          <dd>{this.props.playlist.favourite ? "Yes" : "No"}</dd>
          <dt>Color:</dt>
          <dd style={{ backgroundColor: this.props.playlist.color }}>&nbsp;</dd>
        </dl>
        <input
          type="button"
          value="Edit"
          className="btn btn-info"
          onClick={this.edit}
        />
      </div>
    ) : (
      this.state.playlist && (
        <div>
          <div className="form-group">
            <label>Name:</label>
            <input
              type="text"
              className="form-control"
              value={this.state.playlist.name}
              onChange={this.fieldChange}
              name="name"
            />
          </div>

          <div className="form-group">
            <label>Favourite:</label>
            <input
              type="checkbox"
              checked={this.state.playlist.favourite}
              onChange={this.fieldChange}
              name="favourite"
            />
          </div>

          <div className="form-group">
            <label>Color:</label>
            <input
              type="color"
              value={this.state.playlist.color}
              onChange={this.fieldChange}
              name="color"
            />
          </div>

          <input
            type="button"
            value="Cancel"
            className="btn btn-danger"
            onClick={this.show}
          />
          <input
            type="button"
            value="Save"
            className="btn btn-success"
            onClick={this.save}
          />
        </div>
      )
    );
  }
}
// tsrcc
