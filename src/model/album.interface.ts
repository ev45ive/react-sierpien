interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists: Artist[];
}

interface AlbumImage {
  url: string;
  height: number;
  width: number;
}

interface Artist extends Entity {}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}

export interface PagingObject<T> {
  items: T[];
  total: number;
}
