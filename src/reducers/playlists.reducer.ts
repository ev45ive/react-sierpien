import { Reducer, Action, ActionCreatorsMapObject } from "redux";
import { Playlist } from "src/model/playlist.interface";
import { createSelector } from "reselect";

export interface playlistsState {
  playlists: Playlist[];
  selectedId: Playlist["id"] | null;
  filter: string;
}

// const initialState: playlistsState = {};

export const playlists: Reducer<playlistsState, Actions> = (
  state = {
    playlists: data,
    selectedId: null,
    filter: ""
  },
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_LOADED":
      return { ...state, playlists: action.payload.playlists };

    case "PLAYLIST_SELECTED":
      return { ...state, selectedId: action.payload };

    case "PLAYLISTS_FILTER":
      return { ...state, filter: action.payload };

    case "PLAYLIST_UPDATED":
      return {
        ...state,
        playlists: state.playlists.map(p => {
          return p.id == action.payload.id ? action.payload : p;
        })
      };
    default:
      return state;
  }
};

interface PLAYLISTS_LOADED extends Action<"PLAYLISTS_LOADED"> {
  payload: {
    playlists: Playlist[];
  };
}
interface PLAYLIST_SELECTED extends Action<"PLAYLIST_SELECTED"> {
  payload: Playlist["id"];
}

interface PLAYLIST_UPDATED extends Action<"PLAYLIST_UPDATED"> {
  payload: Playlist;
}
interface PLAYLISTS_FILTER extends Action<"PLAYLISTS_FILTER"> {
  payload: string;
}
type Actions =
  | PLAYLISTS_LOADED
  | PLAYLIST_SELECTED
  | PLAYLIST_UPDATED
  | PLAYLISTS_FILTER;

export const actions: ActionCreatorsMapObject<Actions> = {
  playlistLoaded: (playlists: Playlist[]) => ({
    type: "PLAYLISTS_LOADED",
    payload: { playlists }
  }),

  playlistSelected: (payload: Playlist["id"]) => ({
    type: "PLAYLIST_SELECTED",
    payload
  }),

  playlistUpdated: (payload: Playlist) => ({
    type: "PLAYLIST_UPDATED",
    payload
  }),

  playlistFilter: (payload: string) => ({
    type: "PLAYLISTS_FILTER",
    payload
  })
};

export const selectPlaylistsList = (state: playlistsState) => {
  return state.playlists;
};

// export const selectFilteredPlaylists = (state: playlistsState) => {
//   return state.playlists.filter(playlist =>
//     playlist.name.toLowerCase().match(state.filter.toLocaleLowerCase())
//   );
// };

export const selectFilteredPlaylists = createSelector<
  playlistsState,
  Playlist[],
  string,
  Playlist[]
>(
  state => state.playlists,
  state => state.filter,
  (playlists, filter) => {
    return playlists.filter(playlist =>
      playlist.name.toLowerCase().match(filter.toLocaleLowerCase())
    );
  }
);

export const selectSelectedPlaylist = (state: playlistsState) => {
  return state.playlists.find(p => state.selectedId == p.id);
};

const data = [
  {
    id: 123,
    name: "React HITS!",
    favourite: true,
    color: "#ff00ff"
  },
  {
    id: 234,
    name: "The best of React!",
    favourite: true,
    color: "#ff00ff"
  },
  {
    id: 345,
    name: "React greatest HITS!",
    favourite: true,
    color: "#ff00ff"
  }
];

/* export const playlistLoaded: ActionCreator<PLAYLISTS_LOADED> = () => ({
  type: "PLAYLISTS_LOADED"
});

export const playlistSelected: ActionCreator<PLAYLIST_SELECTED> = () => ({
  type: "PLAYLIST_SELECTED"
});

export const playlistUpdated: ActionCreator<PLAYLIST_UPDATED> = () => ({
  type: "PLAYLIST_UPDATED"
});
 */
