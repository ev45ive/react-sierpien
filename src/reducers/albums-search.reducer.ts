import { Reducer, Action, ActionCreatorsMapObject, Dispatch } from "redux";
import { Album, AlbumsResponse } from "src/model/album.interface";

export interface albumsSearchState {
  albums: Album[];
  query: string;
  isLoading: boolean;
}

interface ALBUM_SEARCH_STARTED extends Action<"ALBUM_SEARCH_STARTED"> {
  payload: {
    query: string;
  };
}
interface ALBUM_SEARCH_LOADED extends Action<"ALBUM_SEARCH_LOADED"> {
  payload: {
    albums: Album[];
  };
}

type Actions = ALBUM_SEARCH_LOADED | ALBUM_SEARCH_STARTED;

export const albums: Reducer<albumsSearchState, Actions> = (
  state = {
    albums: [],
    query: "",
    isLoading: false
  },
  action
) => {
  switch (action.type) {
    case "ALBUM_SEARCH_LOADED":
      return { ...state, albums: action.payload.albums, isLoading: false };

    case "ALBUM_SEARCH_STARTED":
      return { ...state, isLoading: true, query: action.payload.query };
    default:
      return state;
  }
};

export const actions: ActionCreatorsMapObject<Actions> = {
  albumsLoaded: (albums: Album[]) => ({
    type: "ALBUM_SEARCH_LOADED",
    payload: {
      albums
    }
  }),

  startLoading: (query: string) => ({
    type: "ALBUM_SEARCH_STARTED",
    payload: { query }
  })
};

let debounceTimer: NodeJS.Timer;

export const fetchAlbums = (dispatch: Dispatch) => (
  query: string,
  token: string
) => {
  dispatch(actions.startLoading(query));
  // reset timer
  clearTimeout(debounceTimer);

  debounceTimer = setTimeout(() => {
    fetch(`https://api.spotify.com/v1/search?type=album&q=${query}`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(resp => resp.json())
      .then((resp: AlbumsResponse) => resp.albums.items)
      .then(albums => dispatch(actions.albumsLoaded(albums)));
  }, 400);
};

// boundFetch = fetchAlbums(dispatch)
// boundFetch('batman')

export const selectAlbumsList = (state: albumsSearchState) => state.albums;
