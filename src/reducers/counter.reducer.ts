import { Reducer, Action, ActionCreator } from "redux";

type counterState = number;

const initialState: counterState = 0;

type Increment = Action<"INCREMENT">;

type Decrement = Action<"DECREMENT">;

type Actions = Increment | Decrement;

export const increment: ActionCreator<Increment> = () => ({
  type: "INCREMENT"
});

export const decrement: ActionCreator<Decrement> = () => ({
  type: "DECREMENT"
});

export const counter: Reducer<counterState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "INCREMENT":
      return state + 1;
    case "DECREMENT":
      return state - 1;
    default:
      return state;
  }
};
